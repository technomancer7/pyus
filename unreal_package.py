import os

class UnrealPackage:
    def __init__(self):
        self.files = {
            "classes": {},
            "models": {},
            "textures": {},
            "sounds": {}
        }
        self.modifiers = ["final", "static", "simulated", "singular", "protected", "exec", "private"]
        #self.return_types = ["bool", "int", "string"]

    def addFile(self, name, path):
        self.files["classes"][name] = {
            "name": name, 
            "path": path,
            "uc": {
                "extends": "",
                "replication": "",
                "functions": {},
                "comments": [],
                "states": [],
                "header": [],
                "variables": [],
                "default_properties": [],
                "instructions": [],
                "exec": [],
                "loc": 0
            }
        }
        with open(path, "r", encoding = "ISO-8859-1") as f:
            self.files["classes"][name]["body"] = f.read()

    def getFunc(self, ucclass, name):
        return self.files["classes"][ucclass]["uc"]["functions"][name]  

    def decompile(self):
        for name, file in self.files["classes"].items():
            in_func = ""
            fn_depth = 0
            doingdefprops = False
            for ln in file["body"].split("\n"):
                rawln = ln
                ln = ln.strip().lower()
                self.files["classes"][name]["uc"]["loc"] += 1
                if in_func == "" and ("function " in ln or "event " in ln) and (ln.endswith(")") or ln.endswith("{") or ln.endswith(";")) and (not ln.startswith("/*") and not ln.startswith("//") and "=" not in ln and "class" not in ln and not ln.startswith("var")):
                    in_func = ln.split("(")[0].strip().split(" ")[-1]
                    #fn_depth = 1
                    return_type = ""
                    modifiers = []
                    params = []
                    #print("-", ln)
                    for p in ln.split("(")[1].split(")")[0].split(","):
                        if p.strip() != "":
                            params.append(p.strip().split(" "))
                    # find check for extracting values...
                    # maybe manual check this line
                    parts = ln.split("(")[0].split(" ")
                    for item in self.modifiers:
                        if item in parts:
                            modifiers.append(item)
                    while "" in parts: parts.remove("")

                    #print(parts)
                    fn = None
                    if "function" in parts:
                        fn = parts.index("function")

                    if "event" in parts:
                        fn = parts.index("event")

                    if fn != None and parts[fn+1] != in_func:
                        return_type = parts[fn+1]
                    bnative = ln.startswith("native")
                    self.files["classes"][name]["uc"]["functions"][in_func] = {
                        "name": in_func.split(".")[0],
                        "body": "",
                        "param": params,
                        "modifiers": modifiers,
                        "return": return_type,
                        "locals": [],
                        "native": bnative
                    }
                    if ln.endswith(";") or ln.endswith("}"): in_func = ""

                elif in_func == "" and (ln.strip().startswith("var ") or ln.strip().startswith("var(")):
                    self.files["classes"][name]["uc"]["variables"].append(ln.strip())
                elif in_func != "" and ln.strip().startswith("local "):
                    self.files["classes"][name]["uc"]["functions"][in_func]["locals"].append(ln.strip())
                elif in_func != "":
                    if ln.strip() == "}":
                        fn_depth -= 1
                    else:
                        if ln.strip() == "{":
                            fn_depth += 1

                    self.files["classes"][name]["uc"]["functions"][in_func]["body"] += rawln+"\n"

                    if fn_depth == 0:
                        self.files["classes"][name]["uc"]["functions"][in_func]["body"] = self.files["classes"][name]["uc"]["functions"][in_func]["body"].strip()
                        in_func = ""

                elif in_func == "" and ln.startswith("class "+in_func.split('.')[0]):
                    if len(ln.split(" ")) >= 4:
                        self.files["classes"][name]["uc"]["extends"] = ln.split(" ")[3].replace(";", "")
                elif in_func == "" and ln.startswith("#"):
                    self.files["classes"][name]["uc"]["exec"].append(ln)
                elif in_func == "" and ln.strip() == "defaultproperties" and not doingdefprops:
                    doingdefprops = True
                elif doingdefprops and ln.strip() != "{" and ln.strip() != "}" and ln.strip() != "":
                    self.files["classes"][name]["uc"]["default_properties"].append(ln.strip())


    def toJson(self):
        return self.files

class UnrealPackageParser:
    @staticmethod
    def parseUnrealPackage(path, *, logger = None):
        if logger: logger.log("Building index.")
        p = UnrealPackage()
        with os.scandir(path) as it:
            for entry in it:
                if entry.name.lower() == "classes" and entry.is_dir():
                    if logger: logger.log("Entering "+entry.name)
                    UnrealPackageParser.doClasses(p, path+entry.name, logger = logger)
        
        if logger: logger.log("Index built.")
        if logger: logger.log("Parsing codebase...")
        p.decompile()   
        if logger: logger.log("DONE")
        return p        

    @staticmethod
    def doClasses(p, path, *, logger = None):
        with os.scandir(path) as it:
            for entry in it:
                if entry.name.endswith(".uc") and entry.is_file():
                    if logger: logger.log("Entering CLASS "+entry.name)
                    p.addFile(entry.name, entry.path)


