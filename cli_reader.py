import os, json, sys
pkg = sys.argv[1]

if not os.path.exists("json/"+pkg+".json"):
    print("File not found.")
    quit()
data = {}
with open("json/"+pkg+".json") as f:
    data = json.load(f)

stype = sys.argv[3]
sclass = sys.argv[2]

print(stype, sclass)
if data["classes"].get(sclass+".uc"):
    if stype == "vars":
        print('\n'.join((data["classes"][sclass+".uc"]["uc"]["variables"])))
else:
    print("Class not found.")