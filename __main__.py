import os, json, platform
from dearpygui.core import add_file_dialog
import dearpygui.dearpygui as dpg
import dearpygui.logger as dpg_logger
from unreal_package import *
from unreal_server import *

logger = dpg_logger.mvLogger()
#logger.log("This is my logger. Just like an onion it has many levels.")
logid = logger.window_id

default_package_path = "/home/kaiz0r/.wine/drive_c/GOG Games/Deus Ex GOTY/Engine/"
default_out_path = "out.json"

text_viewer_win = "notepad.txt"
text_viewer_nix = "open"

oname = platform.system()

def openfl(file):
    if oname == "Linux":
        os.system(text_viewer_nix+" "+file)
    elif oname == "Windows":
        os.system(text_viewer_win+" "+file)

#TODO in function body, grab local variables for an array
#TODO parse natives in to functions
#TODO get the header of the whole text file
#TODO index the global variables
#TODO extends from, and class modifiers
#TODO parse states
#TODO parse defaultproperties
#TODO parse replication block
#TODO parse compiler instructions (the ones starting with hashtags)
#TODO seperate script for converting the json file output in to a markdown file
 # wine "C:\GOG Games\Deus Ex GOTY\System\Launch.exe" DXMP_RPGCityRemake_3?Game=DXMTL152b1.MTLDeathmatch?Mutator=mPack1.ModifyPlayerSettings -server -LOG=server.log

default_server_launcher = 'wine "C:\GOG Games\Deus Ex GOTY\System\Launch.exe"'
default_server_map = "DXMP_RPGCityRemake_3"
default_server_game = "DXMTL152b1.MTLDeathmatch"
default_server_log = "server.log"


def viewOutput():
    openfl(dpg.get_value(1005))

#from pathlib import Path
def fileBrowse(sender, app_data, user_data):
    print(dpg.get_value(1006))
    print("Sender: ", sender)
    print("App Data: ", app_data)
    print("User Data: ", user_data)
    dpg.set_value(1004, app_data["file_path_name"])
    logger.log("Path updated.")
    #p = Path(app_data["file_path_name")
def browsePkg():
    dpg.add_file_dialog(directory_selector=True, id=1006, callback=fileBrowse, file_count=1, default_path=dpg.get_value(1004))

def runDpkg():
    pkg = dpg.get_value(1004)
    print("Decompiling "+pkg+" to "+dpg.get_value(1005))
    f = UnrealPackageParser.parseUnrealPackage(pkg, logger = logger)

    with open(dpg.get_value(1005), "w+") as fl: fl.write(json.dumps(f.files, sort_keys=True, indent=4))

def main():
    #parser = UnrealPackageParser()
    with dpg.window(label="Doc Generator", width=640, height=130, pos=[0, 130]):
        pass

    with dpg.window(label="Server", width=640, height=130, pos=[0, 260]):
        dpg.add_input_text(label="Package Path", default_value=default_package_path, id=1004)
        dpg.add_input_text(label="Package Path", default_value=default_package_path, id=1004)
        dpg.add_input_text(label="Package Path", default_value=default_package_path, id=1004)

    with dpg.window(label="Script Parser", width=640, height=130):
        dpg.add_text("This function parses Unreal packages and outputs a json file of its data.")
        
        dpg.add_input_text(label="Package Path", default_value=default_package_path, id=1004)
        dpg.add_same_line(spacing=10)
        dpg.add_button(label="...", callback=browsePkg)
        dpg.add_input_text(label="Output File", default_value=default_out_path, id=1005)
        
        dpg.add_button(label="Extract", callback=runDpkg)
        dpg.add_same_line(spacing=10)
        dpg.add_button(label="View Output", callback=viewOutput)


    dpg.setup_viewport()

    dpg.maximize_viewport()
    dpg.set_viewport_title("Pyus")
    dpg.start_dearpygui()


main()