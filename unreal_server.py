import shlex, subprocess

class UnrealServer:
    def __init__(self, *, launcher, map, mutators = [], slog = "server.log", game) -> None:
        self.launcher = launcher
        self.map = map
        self.mutators = mutators
        self.server_log = slog
        self.game = game

    def run(self):
        cm = self.__repr__
        args = shlex.split(cm)
        p = subprocess.Popen(args)
        
    def mutator(self):
        return ','.join(self.mutators)

    def __repr__(self):
        s = f"{self.launcher} {self.map}?Game={self.game}"
        if len(self.mutators) != 0:
            s += f"?Mutator={self.mutator()}"
        s += "-server -LOG=server.log"
        return s
