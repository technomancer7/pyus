from unreal_package import *
import json, sys
pkg = sys.argv[1]
outp = sys.argv[2]

class PsuedoLogger:
    def log(self, ln):
        print(ln)
        
f = UnrealPackageParser.parseUnrealPackage(pkg, logger = PsuedoLogger())

with open(outp+".json", "w+") as fl: fl.write(json.dumps(f.files, sort_keys=True, indent=4))